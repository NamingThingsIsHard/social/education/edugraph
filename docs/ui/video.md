Video
=====

A video page includes (of course) the video itself, but also other diversities.

Ideally, the video should concentrate on a single, main topic.
This will be the new topic to viewer for which new information will be provided
and explained.

Description
-----------
Not much to say, it just describes the video.

Topical metadata
---------------------------

Certain textual metadata should be included on the video page to help the user discover
and understand a topic.
The metadata will be about the things discussed in the video and more.

**Glossary**

All the terms mentioned in the video should be listed here.
It should help users find videos, assess whether the video covers what they wish to learn, etc.

**Required topics for understanding**

Besides the main topic are the topics that won't be explained in the video and that
are thus assumed prior / required knowledge before watching the video.

Depending on [user knowledge], indicators can be provided to help the user estimate
how much of the video they will understand and how much they need to know before viewing can start.

Related videos
--------------

Required topic will all have their own pages and video pages, but their videos can also be shown
in the list of related videos.

Other related videos will include:

- further learning
- dependent videos (videos of dependent topics)
- other videos by the author
- other videos on the same topic

!!! note
    TODO describe how to rank or separate the above? Maybe tabs?

[user knowledge]: ../core/knowledge.md#user-knowledge
