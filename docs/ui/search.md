Search
======

Using the normal search page, users will be able to search for topics and their associated
videos, exercises and tests.

![Search page design](../_static/Graphed.drawio#2)

By default, results are sorted by relevance.

!!! note
    TODO Should the search be differentiated by exercises, videos and tests?
