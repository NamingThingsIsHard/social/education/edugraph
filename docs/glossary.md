# Knowledge graph

## Global graph

All the knowledge available

## Personal graph

What the user knows as visualized on a graph.
It's a subgraph of the global graph.
Nodes can be disconnected.

## Target graph

The nodes a user would like to achieve a certain mastery on.
Nodes can be disconnected.

# Learning path

A list of topics one has to learn in order to get from a starting topic to a target topic.
This need not be the shortest path.

**Example**

When one knows how to stand and would like to learn how to do a backflip,
 one has to learn how to jump, roll in the air, stick a landing.

# Learning graph

The shortest learning paths from one graph to another topic.

**Example**

With the knowledge of how hammering, screwing, and welding, what are all the different topics
 one has to learn in order to build a catapult.

# Knowledge

## Topic

Every node on the graph is a topic that a user can learn.
Each topic has [Required topic]s and is with large likelihood one itself.

**Example**

DNA, Reproduction, for loops,

## Required topic

A [Topic] that is required to be understood in order to understand another.
A topic most likely has many of these.

**Example**

In order to learn pole-vaulting, one must first know how to run.

## Test mastery

How well the user did on topics tests for that topics.
There are two categories and different levels of mastery.

Categories:

  - tested
  - witnessed/vouched for

Levels:

  - Beginner
  - Intermediate
  - Advanced
  - Expert

## Topic mastery

Combination of test mastery and requirement mastery.
How well a user understands a topic taking into consideration how well they understand required topics.

## Requirement mastery

The topic mastery of required topics.

**Example**

You would like to understand DNA.
In order to understand DNA completely you should understand the required topics of 
 acid, polymers, covalent bonds, etc.
You will have mastered (or not) each of those topics.

## Related topics

Topics required in order to understand the selected topic, 
    as well as topics that require the selected topic in order to be understood.

**Example**

In order to understand DNA you should understand acid, polymers, covalent bonds, etc.
In order to understand mitosis you should understand DNA.
