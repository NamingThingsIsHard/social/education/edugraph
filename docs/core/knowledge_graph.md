# Knowledge graph

```dot
digraph virology_by_subject {

    // rankdir=LR;
   compound=true;
   layout=twopi;
   root=virology;
   ranksep=3;
   subgraph cluster_center {
      //   label="Main subject";
      node [fontcolor = blue style=filled lhead=cluster_center]
      virology [fillcolor=yellow label="Virology" peripheries=3];
   }
   virology -> vaccines;

   subgraph cluster_topics {
      label="Topics";
      node [fillcolor=red style=filled peripheries=2]
      "virus structure" -> virology;
      "virus replication" -> virology;
      "virus infection" -> virology;
      "organic chemistry" -> virology;
      "virus detection" -> virology;
   }


   subgraph cluster_requirements {
      label="Requirements";
      node [fillcolor=lightblue style=filled]
      proteins_1 [label="Proteins"]
      proteins_1    -> "virus replication";
      "protein synthesis" -> "virus replication";
      proteins -> "virus structure";
      "cell structure" -> "virus infection";
      //   "SI units" -> virology;
      DNA -> virology;
      RNA_1 [label="RNA"];
      RNA_1 -> "virus structure";
      RNA -> "virus replication";
   }
}
```


Graphs like these will be the main method for users to identify what they need in order to learn
about something.
They represent the [system knowledge].

## Reading knowledge graphs

Here's a simple graph:

```dot
digraph simple_example{

    rankdir=LR;
    main [style=filled fillcolor=yellow label="Virology" peripheries=3];
    subtopic [fillcolor=red style=filled peripheries=2 label="Virus replication"];
    DNA -> subtopic -> main;
}
```

Read from **left to right** this means:
with the understanding of what DNA is, you'll be able to learn about virus replication
which is a part of virology.

Or the other way around from **right to left**:
if you want to learn virology, you'll learn about virus replication for which you'll need to understand DNA.

Using those connections we'll be able to show the relationships between topics.
These relationships can be summed up as the following when reading from right to left

```dot
digraph simple_example{

    rankdir=LR;
    main [style=filled fillcolor=yellow label="Main topic" peripheries=3];
    subtopic [fillcolor=red style=filled peripheries=2 label="Subtopic"];
    main -> subtopic[label="has"];
    subtopic -> Requirement[label="needs"];
}
```
and like this when reading from left to right

```dot
digraph simple_example{

    rankdir=LR;
    main [style=filled fillcolor=yellow label="Main topic" peripheries=3];
    subtopic [fillcolor=red style=filled peripheries=2 label="a topic"];
    start [label="I need to know this"];
    start -> subtopic [label="with which I can learn"];
    subtopic -> main [label="which is part of"];
}
```

With those basics, knowledge can be gathered about any topic, the relationships determined and a graph drawn.

## Uses

Knowledge graphs represent [system knowledge] and the [user knowledge] within it.
Graphs allow us to map paths between nodes, which means, we will be able to see:

- What we need to learn in order to understand a topic
- What we can learn from the things we know

### Paths to knowledge and understanding

We are given this graph with a starting topic and a target topic.

```dot
digraph world_graph{

    start [fillcolor=yellow style=filled label="Start"];
    topic [style=filled fillcolor=red label="Target topic" peripheries=2];

    start -> intertopic1_a
    intertopic1_a -> requirement1
    intertopic1_b -> requirement1
    req1_intertopic1_b -> intertopic1_b
    start -> req1_intertopic1_b

    start -> intertopic2_a
    start -> intertopic2_b
    intertopic2_a -> requirement2
    intertopic2_b -> requirement2

    start -> requirement3

    requirement1 -> topic
    requirement2 -> topic
    requirement3 -> topic

    topic -> topic2

    start -> intertopic3
    other -> intertopic3
    other -> intertopic4

    intertopic3 -> t2_requirement1
    intertopic4 -> t2_requirement2

    t2_requirement1 -> topic2
    t2_requirement2 -> topic2
}
```
These are all the shortest paths one has take from the start to the target if want to cover all requirements.
We call it our learning graph.
This exists because any given topic has required topics one has to understand.

```dot
digraph learning_graph{

    start [fillcolor=yellow style=filled label="Start"];
    topic [style=filled fillcolor=red label="Target topic" peripheries=2];

    start -> intertopic1_a [color="orange"]
    intertopic1_a -> requirement1 [color="orange"]
    intertopic1_b -> requirement1 [color="orange"]
    req1_intertopic1_b -> intertopic1_b [color="orange"]
    start -> req1_intertopic1_b [color="orange"]

    start -> intertopic2_a [color="orange"]
    start -> intertopic2_b [color="orange"]
    intertopic2_a -> requirement2 [color="orange"]
    intertopic2_b -> requirement2 [color="orange"]

    start -> requirement3 [color="orange"]

    requirement1 -> topic [color="orange"]
    requirement2 -> topic [color="orange"]
    requirement3 -> topic [color="orange"]

    topic -> topic2

    start -> intertopic3
    other -> intertopic3
    other -> intertopic4

    intertopic3 -> t2_requirement1
    intertopic4 -> t2_requirement2

    t2_requirement1 -> topic2
    t2_requirement2 -> topic2
}
```
Below is the fastest path in our learning graph.
It's probably the first thing a user wants to see when they ask the system to plot a learning path between two topics.

```dot
digraph fastest_learning_path{

    start [fillcolor=yellow style=filled label="Start"];
    topic [style=filled fillcolor=red label="Target topic" peripheries=2];

    start -> intertopic1_a [color="orange"]
    intertopic1_a -> requirement1 [color="orange"]
    intertopic1_b -> requirement1 [color="orange"]
    req1_intertopic1_b -> intertopic1_b [color="orange"]
    start -> req1_intertopic1_b [color="orange"]

    start -> intertopic2_a [color="orange"]
    start -> intertopic2_b [color="orange"]
    intertopic2_a -> requirement2 [color="orange"]
    intertopic2_b -> requirement2 [color="orange"]

    start -> requirement3 [color="red"]

    requirement1 -> topic [color="orange"]
    requirement2 -> topic [color="orange"]
    requirement3 -> topic [color="red"]

    topic -> topic2

    start -> intertopic3
    other -> intertopic3
    other -> intertopic4

    intertopic3 -> t2_requirement1
    intertopic4 -> t2_requirement2

    t2_requirement1 -> topic2
    t2_requirement2 -> topic2
}
```

Building knowledge graphs
-------------------------

Knowledge graphs are built from the data entered by users about topics (see [Knowledge Collection](knowledge.md#how-is-this-knowledge-collected)).

[system knowledge]: ./knowledge.md#system-knowledge
[user knowledge]: ./knowledge.md#user-knowledge
