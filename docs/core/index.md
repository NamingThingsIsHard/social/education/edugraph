Core concepts
=============


These are the core ideas upon which the app is built.
Hopefully the UI will make these apparent, so this should serve as an in-depth look at them.

The goal is to document them in a manner that will make certain architectural decisions understandable.

As one can read on the main page, the driving mantra behind the website is
*Understanding is just as important as knowing*.

Current school systems in many places around the world focus heavily on knowledge of topics
with a laser focus on memorization.
While those are elements of learning, understanding is often left by the wayside.

This app attempts to provide the tools necessary to help understanding along.
