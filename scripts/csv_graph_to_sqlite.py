"""
graphed_education
Copyright (C) 2023 LoveIsGrief

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import argparse
import csv
import sqlite3

from typing.io import TextIO


def main(csv_path: str, sql_lite_path):
    """
    Optimizations from https://stackoverflow.com/q/1711631
    """
    con = sqlite3.connect(sql_lite_path)
    # Store the journal in memory instead of writing it to disk
    con.execute("PRAGMA journal_mode = MEMORY")
    setup_db(con)
    curr_percent = 0
    count = 0
    # Use one large transaction to write the DB
    con.execute("BEGIN TRANSACTION")
    with open(csv_path, "r") as csv_file, con:
        total = get_total(csv_file)
        reader = csv.reader(csv_file)
        print("Start")
        for target_node, source_node in reader:
            insert_into_graph(source_node, target_node, con)
            count += 1
            new_curr_percent = int((count / total) * 100)
            if new_curr_percent != curr_percent:
                curr_percent = new_curr_percent
                print(curr_percent)
    print("Done")


def setup_db(con: sqlite3.Connection):
    con.execute("""
    CREATE TABLE IF NOT EXISTS nodes (
        id INTEGER PRIMARY KEY ASC AUTOINCREMENT,
        name VARCHAR(255) UNIQUE NOT NULL
    )
    """)

    con.execute("""
    CREATE TABLE IF NOT EXISTS graph (
        source, target,
        PRIMARY KEY(source, target),
        FOREIGN KEY(source, target) REFERENCES nodes (id, id)
    )
    """)


def get_total(file: TextIO):
    total = 0
    for _ in file:
        total += 1
    file.seek(0)
    return total


def insert_into_graph(source: str, target: str, con: sqlite3.Connection):
    con.executemany("""
    INSERT or ignore INTO nodes (name) VALUES (?)
    """, [
        (source,),
        (target,),
    ])
    con.execute("""
    INSERT or ignore INTO graph
        SELECT source.id, target.id
        FROM nodes as source, nodes as target
        WHERE
            source.name = ? AND target.name = ?        
    """, [source, target])


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("csv_file")
    parser.add_argument("db_file")

    args = parser.parse_args()

    main(args.csv_file, args.db_file)
