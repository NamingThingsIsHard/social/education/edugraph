Feature: Target graph feature for users

  Users would like to learn topics or entire fields.
  These are called target graphs.
  Users should be able to interact with these somehow.

  Scenario: Viewing a target graph
    Given a user has a target graph
    When the user views the target graph
    Then they should see
      * the target graph itself
      * paths to the target graph
      * a table of topics with TTLs and distances to personal graph
      * a schedule planner

  Scenario: Visualizing learning paths on the global graph
    Given a user is viewing a graph
    When then user selects a topic and the topic:
      * doesn't belong to the personal graph
    Then the user should see the shortest path to the target topic

  Scenario: Schedule planner for target graphs
    Given a user has a target graph
    When the user views the schedule planner of a target graph
    Then they should see a calendar allowing them to plan when they will tackle certain topics
