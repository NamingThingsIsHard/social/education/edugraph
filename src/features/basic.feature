Feature: Basic Features

  Scenario: User opens their graph
    When a user opens their graph page
    Then a graph with their personal graph should be visible


  Scenario: User checks a topic
    Given a user and the global graph
    When the user views the information on the topic
    Then the use should see
      * Topic mastery
      * Estimated TTL for higher masteries
      * Average TTL for all masteries
      * Related topics
      * Suggested topics
      * Learning materials

  Scenario: User checks TTL for subgraph
    # TODO define this

  Scenario: User wants to see how well they understand a topic
    Given A user has a selected topic
    When The user open the mastery information view
    Then The user should get an explanation of how their mastery level is calculated
      * test evaluation
      * dependency mastery
